#include <iostream>
#include <array>
using namespace std;

class animal
{
public:
	virtual void voice()
		const = 0;
	virtual ~animal() {}
};

class dog : virtual public animal
{
	void voice() const override
	{
		puts("woof!");
	}
};

class cat : virtual public animal
{
	void voice() const override
	{
		puts("meow!");
	}
};

class mouse : virtual public animal
{
	void voice() const override
	{
		puts("crack!");
	}
};

int main()
{
	dog a1, a2;
	cat b1, b2;
	mouse c1, �2;

	array <animal*, 6> animals{&a1,&a2, &b1, &b2, &c1, &�2,};

	for (const auto& p : animals)
	p->voice();
}